package HomeworkCurs4;

public class Main {
    public static void main(String[] args) {

        Author x = new Author();
        Author y = new Author();

        x.name = "Vonnegut";
        x.email = "von@von.com";

        y.name = "Asimov";
        y.email = "asi@asi.com";

        Book a = new Book();
        Book b = new Book();

        a.name = "Leaganul pisicii";
        a.author = " Vonnegut";
        a.price = 20.00;
        a.year = 2019;

        b.name = "Fundatia";
        b.author = " Asimov";
        b.price = 25.00;
        b.year = 2020;

        System.out.println("Book " + a.name + "(" + a.price + " RON " + ")" + "," + "by" + a.author + "," + "published in year" + a.year);
        System.out.println("Book " + b.name + "(" + b.price + " RON " + ")" + "," + "by" + b.author + "," + "published in year" + b.year);


    }
}
