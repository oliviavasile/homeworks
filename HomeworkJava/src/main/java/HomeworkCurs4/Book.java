package HomeworkCurs4;

public class Book {

    String name;
    String author;
    Double price;
    int year;

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public Double getPrice() {
        return price;
    }

    public int getYear() {
        return year;
    }
}
